package fr.soleil.comete.simulated.data.source;

import fr.soleil.comete.simulated.data.service.SimulatedKey;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;

public abstract class AbstractSimulatedDataSource<T> extends AbstractDataSource<T> {

    private final String threadName;

    public AbstractSimulatedDataSource(IKey key) {
        super(key);
        threadName = this + ": notify mediators every second";
        if ((key instanceof SimulatedKey) && ((SimulatedKey) key).isRefreshed()) {
            Thread thread = new Thread(threadName) {
                @Override
                public void run() {
                    while (true) {
                        notifyMediators();
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                        }
                    }
                }
            };
            thread.start();
        }
    }

    @Override
    public boolean isSettable() {
        boolean settable = false;
        if (originDescriptor instanceof SimulatedKey) {
            settable = ((SimulatedKey) originDescriptor).isSettable();
        }
        return settable;
    }
}
