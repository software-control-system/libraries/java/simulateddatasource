//+============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/BooleanDAO.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.source.scalar;

import java.util.Random;

import fr.soleil.comete.simulated.data.source.AbstractSimulatedDataSource;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;

public class BooleanRandomDataSource extends AbstractSimulatedDataSource<Boolean> {

    private static Random random = new Random();

    public BooleanRandomDataSource(IKey key) {
        super(key);
    }

    @Override
    public Boolean getData() {
        return random.nextBoolean();
    }

    @Override
    public void setData(Boolean avalue) {
        // do nothing
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(Boolean.class);
    }
}
