// +============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/DoubleDAO.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.source.matrix;

import javax.activation.UnsupportedDataTypeException;

import fr.soleil.comete.simulated.data.source.AbstractSimulatedDataSource;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.container.matrix.BooleanMatrix;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;

public class BooleanMatrixDataSource extends AbstractSimulatedDataSource<AbstractMatrix<Boolean>> {

    AbstractMatrix<Boolean> value = new BooleanMatrix();

    public BooleanMatrixDataSource(IKey key) {
        super(key);

        try {
            value.setFlatValue(new boolean[] { false, true, false, true, false, true }, 3, 2);
        } catch (UnsupportedDataTypeException e) {
            e.printStackTrace();
        }
    }

    @Override
    public AbstractMatrix<Boolean> getData() {
        return value;
    }

    @Override
    public void setData(AbstractMatrix<Boolean> avalue) {
        if (avalue != null) {
            value = avalue;
            notifyMediators();
        }
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(AbstractMatrix.class,
                new GenericDescriptor[] { new GenericDescriptor(Boolean.TYPE) });
    }

}
