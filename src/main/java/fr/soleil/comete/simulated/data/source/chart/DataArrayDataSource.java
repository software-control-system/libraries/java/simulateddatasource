// +============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/DoubleDataSource.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.source.chart;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.simulated.data.source.AbstractSimulatedDataSource;
import fr.soleil.comete.simulated.data.source.polled.FormatSource;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;

public class DataArrayDataSource extends AbstractSimulatedDataSource<Map<String, Object>> implements FormatSource {

    private final Map<String, Object> dataMap = new LinkedHashMap<String, Object>();

    private static final Random random = new Random();
    private static final int arraySize = 30;
    private static final int nbOfArray = 10;

    /**
     * @param arrayList
     */
    public DataArrayDataSource(IKey key) {
        super(key);
        for (int i = 0; i < nbOfArray; i++) {
            double[][] dataArray = new double[2][arraySize];
            fillArray(dataArray);
            dataMap.put("Data " + i, dataArray);
        }
    }

    private void fillArray(double[][] dataArray) {

        for (int i = 0; i < arraySize; i++) {
            dataArray[IChartViewer.X_INDEX][i] = i + 1;
            dataArray[IChartViewer.Y_INDEX][i] = random.nextDouble();
        }
    }

    private Map<String, Object> generateData() {

        Iterator<String> listIterator = dataMap.keySet().iterator();
        Map<String, Object> tempDataMap = new LinkedHashMap<String, Object>();

        while (listIterator.hasNext()) {
            String id = listIterator.next();
            Object array = dataMap.get(id);

            if (array instanceof double[][]) {

                double[][] dataArray = (double[][]) array;

                for (int i = 0; i < dataArray[0].length; ++i) {
                    dataArray[IChartViewer.Y_INDEX][i] = i;
                    dataArray[IChartViewer.Y_INDEX][i] = random.nextDouble();
                }

                tempDataMap.put(id, dataArray);
            }
        }

        return tempDataMap;
    }

    @Override
    public Map<String, Object> getData() {
        return generateData();
    }

    @Override
    public void setData(Map<String, Object> avalue) {
        // Nothing todo
    }

    @Override
    public String getFormat() {
        return "%6.4f";
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(Map.class, new GenericDescriptor[] { new GenericDescriptor(String.class),
            new GenericDescriptor(Object.class) });
    }

}
