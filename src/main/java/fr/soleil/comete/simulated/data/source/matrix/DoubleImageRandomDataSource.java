//+============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/DoubleReadOnlyDataSource.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.source.matrix;

import fr.soleil.data.service.IKey;

public class DoubleImageRandomDataSource extends AbstractDoubleMatrixRandomDataSource {

    public DoubleImageRandomDataSource(IKey key) {
        super(key, 300, 200);
    }
}
