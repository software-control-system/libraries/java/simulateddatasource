//+============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/DoubleDataSource.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.source.scalar;

import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;

public class ShortDataSource extends AbstractNumberDataSource<Short> {
    private Short value = Short.valueOf((short) 0);

    public ShortDataSource(IKey key) {
        super(key);
    }

    @Override
    public Short getData() {
        return value;
    }

    @Override
    public void setData(Short avalue) {
        if (avalue.shortValue() != value.shortValue()) {
            value = avalue;
            notifyMediators();
        }
    }

    public String getFormat() {
        return "%2d";
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(Short.class);
    }
}
