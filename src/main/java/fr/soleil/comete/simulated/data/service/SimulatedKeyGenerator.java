package fr.soleil.comete.simulated.data.service;

import java.awt.List;
import java.util.Map;

import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;

public class SimulatedKeyGenerator implements IKeyGenerator {

    public static final String BOOLEAN_SCALAR_STRING = "simulated boolean scalar";
    public static final String BOOLEAN_SCALAR_RANDOM = "simulated boolean scalar random";
    public static final String BOOLEAN_MATRIX_STRING = "simulated boolean matrix";
    public static final String STRING_SCALAR_STRING = "simulated string scalar";
    public static final String STRING_MATRIX_STRING = "simulated string matrix";
    public static final String NUMBER_SCALAR_STRING = "simulated number scalar";
    public static final String NUMBER_SCALAR_RANDOM = "simulated number scalar random";
    public static final String NUMBER_SPECTRUM_STRING = "simulated number spectrum";
    public static final String NUMBER_MATRIX_RANDOM = "simulated number matrix random";

    public SimulatedKeyGenerator() {
    }

    @Override
    public IKey getBooleanMatrixKey() {
        SimulatedKey key = new SimulatedKey();
        key.setInformationKey(BOOLEAN_MATRIX_STRING);
        key.setSettable(true);
        key.registerRandom(false);
        key.registerRefreshed(false);
        key.setExpectedSourceType(new GenericDescriptor(AbstractMatrix.class,
                new GenericDescriptor[] { new GenericDescriptor(Boolean.class) }));
        return key;
    }

    public IKey getBooleanRandomScalarKey() {
        SimulatedKey key = new SimulatedKey();
        key.setSettable(false);
        key.registerRandom(true);
        key.setInformationKey(BOOLEAN_SCALAR_RANDOM);
        key.setExpectedSourceType(new GenericDescriptor(Boolean.class));
        return key;
    }

    @Override
    public IKey getBooleanScalarKey() {
        SimulatedKey key = new SimulatedKey();
        key.setSettable(true);
        key.setInformationKey(BOOLEAN_SCALAR_STRING);
        key.setExpectedSourceType(new GenericDescriptor(Boolean.class));
        return key;
    }

    @Override
    public IKey getChartKey() {
        SimulatedKey key = new SimulatedKey();
        key.setSettable(false);
        key.registerRandom(true);
        key.setExpectedSourceType(new GenericDescriptor(Map.class, new GenericDescriptor[] {
            new GenericDescriptor(String.class), new GenericDescriptor(Object.class) }));
        return key;
    }

    @Override
    public IKey getSpectrumNumberMatrixKey() {
        SimulatedKey key = new SimulatedKey();
        key.setInformationKey(NUMBER_SPECTRUM_STRING);
        key.setSettable(false);
        key.registerRandom(true);
        key.setExpectedSourceType(new GenericDescriptor(AbstractMatrix.class,
                new GenericDescriptor[] { new GenericDescriptor(Number.class) }));
        key.setSpectrum(true);
        return key;
    }

    @Override
    public IKey getImageNumberMatrixKey() {
        SimulatedKey key = new SimulatedKey();
        key.setSettable(false);
        key.setInformationKey(NUMBER_MATRIX_RANDOM);
        key.setExpectedSourceType(new GenericDescriptor(AbstractMatrix.class,
                new GenericDescriptor[] { new GenericDescriptor(Number.class) }));
        return key;
    }

    public IKey getNumberSimpleScalarKey() {
        SimulatedKey key = new SimulatedKey();
        key.setSettable(true);
        key.setInformationKey(NUMBER_SCALAR_STRING);
        key.setExpectedSourceType(new GenericDescriptor(Number.class));
        return key;
    }

    @Override
    public IKey getNumberScalarKey() {
        SimulatedKey key = new SimulatedKey();
        key.setInformationKey(NUMBER_SCALAR_RANDOM);
        key.registerRandom(true);
        key.setSettable(false);
        key.setExpectedSourceType(new GenericDescriptor(Number.class));
        return key;
    }

    @Override
    public IKey getStringMatrixKey() {
        SimulatedKey key = new SimulatedKey();
        key.setInformationKey(STRING_MATRIX_STRING);
        key.setSettable(true);
        key.setExpectedSourceType(new GenericDescriptor(AbstractMatrix.class,
                new GenericDescriptor[] { new GenericDescriptor(String.class) }));
        return key;
    }

    @Override
    public IKey getStringScalarKey() {
        SimulatedKey key = new SimulatedKey();
        key.setInformationKey(STRING_SCALAR_STRING);
        key.setSettable(true);
        key.setExpectedSourceType(new GenericDescriptor(String.class));
        return key;
    }

    @Override
    public String toString() {
        return SimulatedDataSourceProducer.SOURCE_PRODUCER_ID;
    }

    @Override
    public IKey getStringScalarKey2() {
        return getStringScalarKey();
    }

    @Override
    public IKey getNumberMatrixListKey() {
        SimulatedKey key = new SimulatedKey();
        key.setExpectedSourceType(new GenericDescriptor(List.class,
                new GenericDescriptor[] { new GenericDescriptor(AbstractMatrix.class,
                        new GenericDescriptor[] { new GenericDescriptor(String.class) }) }));
        return key;
    }

    @Override
    public IKey getChartListKey() {
        SimulatedKey key = new SimulatedKey();
        key.setExpectedSourceType(new GenericDescriptor(
                List.class,
                new GenericDescriptor[] { new GenericDescriptor(Map.class, new GenericDescriptor[] {
                    new GenericDescriptor(String.class), new GenericDescriptor(Object.class) }) }));
        return key;
    }
}
