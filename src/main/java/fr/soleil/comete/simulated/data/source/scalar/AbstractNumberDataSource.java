package fr.soleil.comete.simulated.data.source.scalar;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.comete.simulated.data.plugin.SimulatedQualityPlugin;
import fr.soleil.comete.simulated.data.service.SimulatedDataSourceProducer;
import fr.soleil.comete.simulated.data.source.AbstractSimulatedDataSource;
import fr.soleil.comete.simulated.data.source.polled.FormatSource;
import fr.soleil.data.plugin.AbstractPlugin;
import fr.soleil.data.plugin.FormatPlugin;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.IPluggableDataSource;

public abstract class AbstractNumberDataSource<T> extends AbstractSimulatedDataSource<T> implements
IPluggableDataSource, FormatSource {

    protected final List<AbstractPlugin<?>> plugins;

    public AbstractNumberDataSource(IKey key) {
        super(key);
        plugins = new ArrayList<AbstractPlugin<?>>();
    }

    @Override
    public List<AbstractPlugin<?>> getListPlugin() {
        List<AbstractPlugin<?>> result = new ArrayList<AbstractPlugin<?>>();
        synchronized (plugins) {
            if (plugins.isEmpty()) {
                initPlugins();
            }
            result.addAll(plugins);
        }
        return result;
    }

    /**
     * Initializes the plugin list
     */
    protected void initPlugins() {
        plugins.add(new FormatPlugin(SimulatedDataSourceProducer.GENERATOR.getStringScalarKey()));
        plugins.add(new SimulatedQualityPlugin(getOriginDescriptor()));
    }
}
