// +============================================================================
//Source: package fr.soleil.comete.dao.util;/DefaultKey.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.service;

import java.util.Map;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.exception.KeyCompatibilityException;
import fr.soleil.data.service.AbstractKey;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.xml.GenericDescriptorXmlManager;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;

public class SimulatedKey extends AbstractKey {

    public static final String SIMULATED_DESCRIPTOR = "Simulated Descriptor";
    public static final String SIMULATED_FORMAT = "Simulated Format";
    public static final String SIMULATED_COLORATION = "Simulated Coloration";
    public static final String SIMULATED_TREE = "Simulated Tree";
    public static final String SIMULATED_SPECTRUM = "Simulated spectrum";
    protected static final String SOURCE_TYPE_XML_TAG = "sourceType";
    protected static final String SIMULATED_DESCRIPTOR_XML_TAG = "simulatedDescriptor";
    protected static final String SIMULATED_FORMAT_XML_TAG = "simulatedFormat";
    protected static final String SIMULATED_COLORATION_XML_TAG = "simulatedColoration";
    protected static final String SIMULATED_TREE_XML_TAG = "simulatedTree";
    protected static final String SIMULATED_SPECTRUM_XML_TAG = "simulatedSpectrum";
    public static final String PROPERTY_NAME_REFRESHED = "Refreshed";
    public static final String PROPERTY_NAME_RANDOM = "Random";
    private String informationKey = SimulatedDataSourceProducer.SOURCE_PRODUCER_ID;
    private boolean settable = false;

    static {
        DataSourceProducerProvider.pushNewProducer(SimulatedDataSourceProducer.class);
    }

    private GenericDescriptor expectedSourceType;

    public SimulatedKey() {
        super(SimulatedDataSourceProducer.SOURCE_PRODUCER_ID);
    }

    public void setSettable(boolean settable) {
        this.settable = settable;
    }

    public boolean isSettable() {
        return settable;
    }

    public void setInformationKey(String informationKey) {
        this.informationKey = informationKey;
    }

    @Override
    public String getInformationKey() {
        return informationKey;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    public void registerRefreshed(Boolean refreshed) {
        registerProperty(PROPERTY_NAME_REFRESHED, refreshed);
    }

    public boolean isRefreshed() {
        boolean refresh = true;
        Object result = getPropertyValue(PROPERTY_NAME_REFRESHED);
        if (result instanceof Boolean) {
            refresh = (Boolean) result;
        }
        return refresh;
    }

    public void registerRandom(Boolean random) {
        registerProperty(PROPERTY_NAME_RANDOM, random);
    }

    public boolean isRandom() {
        boolean random = true;
        Object result = getPropertyValue(PROPERTY_NAME_RANDOM);
        if (result instanceof Boolean) {
            random = (Boolean) result;
        }
        return random;
    }

    public static boolean isADescriptor(IKey key) {
        boolean result = false;
        Object tempResult = key.getPropertyValue(SIMULATED_DESCRIPTOR);
        if ((tempResult != null) && (tempResult instanceof GenericDescriptor)) {
            result = true;
        }
        return result;
    }

    public static void registerDescriptor(IKey key, GenericDescriptor descriptor) {
        key.registerProperty(SIMULATED_DESCRIPTOR, descriptor);
    }

    public static void registerTreeKey(IKey key) {
        key.registerProperty(SIMULATED_TREE, true);
    }

    public static boolean isTreeKey(IKey key) {
        Boolean value = (Boolean) key.getPropertyValue(SIMULATED_TREE);
        if (value == null) {
            value = false;
        }
        return value.booleanValue();
    }

    public void setSpectrum(boolean spectrum) {
        registerProperty(SIMULATED_SPECTRUM, spectrum);
    }

    public boolean isSpectrum() {
        Boolean value = (Boolean) getPropertyValue(SIMULATED_SPECTRUM);
        if (value == null) {
            value = false;
        }
        return value.booleanValue();
    }

    public static GenericDescriptor getDescriptor(IKey key) {
        GenericDescriptor result = null;
        Object tempResult = key.getPropertyValue(SIMULATED_DESCRIPTOR);
        if ((tempResult != null) && (tempResult instanceof GenericDescriptor)) {
            result = (GenericDescriptor) tempResult;
        }
        return result;
    }

    public static void registerColoration(IKey key, ColoringLevel level, GenericDescriptor descriptor) {
        registerDescriptor(key, descriptor);
        key.registerProperty(SIMULATED_COLORATION, level);
    }

    public static void registerFormat(IKey key, GenericDescriptor descriptor) {
        registerDescriptor(key, descriptor);
        key.registerProperty(SIMULATED_FORMAT, true);
    }

    public static boolean isAQuality(IKey key) {
        boolean result = false;
        ColoringLevel tempResult = (ColoringLevel) key.getPropertyValue(SIMULATED_COLORATION);
        if (tempResult != null) {
            result = (tempResult == ColoringLevel.QUALITY);
        }
        return result;
    }

    public static boolean isAState(IKey key) {
        boolean result = false;
        ColoringLevel tempResult = (ColoringLevel) key.getPropertyValue(SIMULATED_COLORATION);
        if (tempResult != null) {
            result = (tempResult == ColoringLevel.STATE);
        }
        return result;
    }

    public static boolean isAFormat(IKey key) {
        boolean result = false;
        Boolean tempResult = (Boolean) key.getPropertyValue(SIMULATED_FORMAT);
        if (tempResult != null) {
            result = tempResult.booleanValue();
        }
        return result;
    }

    /**
     * @param expectedSourceType the expectedSourceType to set
     */
    public void setExpectedSourceType(GenericDescriptor expectedSourceType) {
        this.expectedSourceType = expectedSourceType;
    }

    /**
     * @return the expectedSourceType
     */
    public GenericDescriptor getExpectedSourceType() {
        return expectedSourceType;
    }

    @Override
    public String toString() {
        return expectedSourceType.toString();
    }

    @Override
    public XMLLine[] toXMLLines(String tag) {
        XMLLine openingLine = createOpeningOrEmptyLine(tag, false);
        XMLLine closingLine = new XMLLine(tag, XMLLine.CLOSING_TAG_CATEGORY);
        Boolean tempFormat = (Boolean) getPropertyValue(SIMULATED_FORMAT);
        if ((tempFormat != null) && tempFormat.booleanValue()) {
            openingLine.setAttribute(SIMULATED_FORMAT_XML_TAG, Boolean.toString(true));
        }
        ColoringLevel tempColoration = (ColoringLevel) getPropertyValue(SIMULATED_COLORATION);
        if (tempColoration != null) {
            openingLine.setAttribute(SIMULATED_COLORATION_XML_TAG, tempColoration.toString());
        }
        Boolean tempSpectrum = (Boolean) getPropertyValue(SIMULATED_SPECTRUM);
        if ((tempSpectrum != null) && tempSpectrum.booleanValue()) {
            openingLine.setAttribute(SIMULATED_SPECTRUM_XML_TAG, Boolean.toString(true));
        }
        Boolean tempTree = (Boolean) getPropertyValue(SIMULATED_TREE);
        if ((tempTree != null) && tempTree.booleanValue()) {
            openingLine.setAttribute(SIMULATED_TREE_XML_TAG, Boolean.toString(true));
        }
        XMLLine[] sourceType = GenericDescriptorXmlManager.toXMLLines(SOURCE_TYPE_XML_TAG, expectedSourceType);
        XMLLine[] descriptor = GenericDescriptorXmlManager.toXMLLines(SIMULATED_DESCRIPTOR_XML_TAG,
                (GenericDescriptor) getPropertyValue(SIMULATED_DESCRIPTOR));
        int count = 2;
        if (sourceType != null) {
            count += sourceType.length;
        }
        if (descriptor != null) {
            count += descriptor.length;
        }
        XMLLine[] result = new XMLLine[count];
        result[0] = openingLine;
        result[result.length - 1] = closingLine;
        int index = 1;
        if (sourceType != null) {
            System.arraycopy(sourceType, 0, result, index, sourceType.length);
            index += sourceType.length;
        }
        if (descriptor != null) {
            System.arraycopy(descriptor, 0, result, index, descriptor.length);
            index += descriptor.length;
        }
        return result;
    }

    @Override
    public void parseXML(Node node) throws KeyCompatibilityException {
        if (node == null) {
            throw new KeyCompatibilityException("Can't parse a null XML node");
        } else {
            Map<String, String> propertyMap = checkKeyCompatibility(node, getClass());
            propertiesTable.clear();
            registerProperty(SOURCE_PRODUCTION_PROPERTY_NAME, SimulatedDataSourceProducer.SOURCE_PRODUCER_ID);
            String tempFormat = propertyMap.get(SIMULATED_FORMAT_XML_TAG);
            String tempColoration = propertyMap.get(SIMULATED_COLORATION_XML_TAG);
            String tempSpectrum = propertyMap.get(SIMULATED_SPECTRUM_XML_TAG);
            String tempTree = propertyMap.get(SIMULATED_TREE_XML_TAG);
            expectedSourceType = null;
            GenericDescriptor descriptor = null;
            if (node.hasChildNodes()) {
                NodeList subNodes = node.getChildNodes();
                for (int i = 0; i < subNodes.getLength(); i++) {
                    Node subNode = subNodes.item(i);
                    if (!XMLUtils.isAFakeNode(subNode)) {
                        String subNodeType = subNode.getNodeName().trim();
                        if (SOURCE_TYPE_XML_TAG.equals(subNodeType)) {
                            try {
                                expectedSourceType = GenericDescriptorXmlManager.parseXML(subNode);
                            } catch (DataAdaptationException e) {
                                throw new KeyCompatibilityException("Failed to extract source type", e);
                            }
                        } else if (SIMULATED_DESCRIPTOR_XML_TAG.equals(subNodeType)) {
                            try {
                                descriptor = GenericDescriptorXmlManager.parseXML(subNode);
                            } catch (DataAdaptationException e) {
                                throw new KeyCompatibilityException("Failed to extract simulated descriptor", e);
                            }
                        }
                    }
                }
            }
            if (tempSpectrum != null) {
                setSpectrum("true".equalsIgnoreCase(tempSpectrum.trim()));
            }
            if (tempTree != null) {
                boolean tree = "true".equalsIgnoreCase(tempTree.trim());
                if (tree) {
                    registerTreeKey(this);
                }
            }
            if (descriptor != null) {
                registerDescriptor(this, descriptor);
                if (tempFormat != null) {
                    if ("true".equalsIgnoreCase(tempFormat.trim())) {
                        registerFormat(this, descriptor);
                    }
                }
                if (tempColoration != null) {
                    for (ColoringLevel level : ColoringLevel.values()) {
                        if (tempColoration.equals(level.toString())) {
                            registerColoration(this, level, descriptor);
                            break;
                        }
                    }
                }
            }
        }

    }
}
