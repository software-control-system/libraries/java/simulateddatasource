// +============================================================================
// Source: package fr.soleil.comete.dao.simulateddao;/DoubleReadOnlyDataSource.java
//
// project : Comete
//
// Description: This class hides
//
// Author: SAINTIN
//
// Revision: 1.1
//
// Log:
//
// copyleft :Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
// FRANCE
//
// +============================================================================
package fr.soleil.comete.simulated.data.source.matrix;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.comete.simulated.data.source.AbstractSimulatedDataSource;
import fr.soleil.comete.simulated.data.source.polled.FormatSource;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;

public class StackMatrixRandomDataSource extends AbstractSimulatedDataSource<List<AbstractNumberMatrix<?>>>
implements FormatSource {

    private final List<AbstractNumberMatrix<?>> stackMatrice = new ArrayList<AbstractNumberMatrix<?>>();
    private int listSize = 1;

    public StackMatrixRandomDataSource(IKey key) {
        super(key);
        if (stackMatrice.isEmpty()) {
            for (int i = 0; i < listSize; i++) {
                stackMatrice.add(new DoubleImageStaticDataSource(null, "Image " + i).getData());
            }
        }
    }

    @Override
    public List<AbstractNumberMatrix<?>> getData() {
        return stackMatrice;
    }

    @Override
    public void setData(List<AbstractNumberMatrix<?>> avalue) {
        // Nothing to do
    }

    public int getListSize() {
        return listSize;
    }

    public void setListSize(int size) {
        listSize = size;
        stackMatrice.clear();
        if (stackMatrice.isEmpty()) {
            for (int i = 0; i < listSize; i++) {
                stackMatrice.add(new DoubleImageStaticDataSource(null, "Image " + i).getData());
            }
        }
    }


    public String getFormat() {
        return "%d";
    }

    @Override
    public GenericDescriptor getDataType() {
        GenericDescriptor numberDesc = new GenericDescriptor(AbstractNumberMatrix.class,
                new GenericDescriptor[] { new GenericDescriptor(Number.class) });

        return new GenericDescriptor(List.class, new GenericDescriptor[] { numberDesc });
    }

}
