// +============================================================================
// Source: package fr.soleil.comete.dao.simulateddao;/DoubleReadOnlyDataSource.java
//
// project : Comete
//
// Description: This class hides
//
// Author: SAINTIN
//
// Revision: 1.1
//
// Log:
//
// copyleft :Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
// FRANCE
//
// +============================================================================
package fr.soleil.comete.simulated.data.source.matrix;

import java.util.Random;

import javax.activation.UnsupportedDataTypeException;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.simulated.data.source.AbstractSimulatedDataSource;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.container.matrix.DoubleMatrix;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.ObjectUtils;

public class DoubleImageStaticDataSource extends AbstractSimulatedDataSource<AbstractNumberMatrix<Double>> {

    private final AbstractNumberMatrix<Double> matrix = new DoubleMatrix();

    private static final int DEFAULT_WIDTH = 250, DEFAULT_HEIGHT = 240;

    private final int width;
    private final int height;
    private final double[] attr_beam_image_read;
    private static Random random = new Random();

    public DoubleImageStaticDataSource(IKey key, int width, int height) {
        this(key, ObjectUtils.EMPTY_STRING, width, height);
    }

    public DoubleImageStaticDataSource(IKey key) {
        this(key, ObjectUtils.EMPTY_STRING, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public DoubleImageStaticDataSource(IKey key, String name) {
        this(key, name, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public DoubleImageStaticDataSource(IKey key, String name, int width, int height) {
        super(key);
        matrix.setName(name);
        this.width = Math.max(1, width);
        this.height = Math.max(1, height);
        attr_beam_image_read = new double[this.width * this.height];
        generate_image();
    }

    @Override
    public AbstractNumberMatrix<Double> getData() {
        return matrix;
    }

    @Override
    public void setData(AbstractNumberMatrix<Double> avalue) {
        // Nothing todo
    }

    private void generate_image() {
        int max_xy = width;

        int bimg_center_x = random.nextInt() % (int) (max_xy * 0.050000000000000003D);
        if (random.nextLong() % 2L == 0L) {
            bimg_center_x *= -1;
        }
        int bimg_center_y = random.nextInt() % (int) (max_xy * 0.050000000000000003D);
        if (random.nextInt() % 2 == 0) {
            bimg_center_y *= -1;
        }
        int bimg_offset_to_zero = (max_xy - 1) / 2;
        int bimg_x_offset_to_zero = bimg_offset_to_zero + bimg_center_x;
        int bimg_y_offset_to_zero = bimg_offset_to_zero + bimg_center_y;
        int limit = max_xy / 8;
        int noise = random.nextInt() % (int) (limit * 0.20000000000000001D);
        if (random.nextInt() % 2 == 0) {
            noise *= -1;
        }
        limit += noise;
        for (int cpt = 0; cpt < width * height; cpt++) {
            attr_beam_image_read[cpt] = 0;
        }

        for (int i = -limit; i < limit; i++) {
            int y = i + bimg_y_offset_to_zero;
            if ((y >= 0) && (y < max_xy)) {
                for (int j = -limit; j < limit; j++) {
                    int x = j + bimg_x_offset_to_zero;
                    if ((x >= 0) && (x < max_xy)) {
                        int value = (int) Math.sqrt(i * i + j * j);
                        attr_beam_image_read[x * max_xy + y] = value >= limit ? 0 : limit - value;
                    }
                }

            }
        }

        try {
            matrix.setFlatValue(attr_beam_image_read, height, width);
        } catch (UnsupportedDataTypeException e) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to generate random image", e);
        }
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(AbstractNumberMatrix.class,
                new GenericDescriptor[] { new GenericDescriptor(Double.class) });
    }

}
