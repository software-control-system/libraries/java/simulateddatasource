// +============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/StringDataSource.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.source.scalar;

import fr.soleil.comete.simulated.data.source.AbstractSimulatedDataSource;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.ObjectUtils;

public class StringDataSource extends AbstractSimulatedDataSource<String> {

    private static final String UNKNOWN = "UNKNOWN";
    private String value = ObjectUtils.EMPTY_STRING;
    private final String threadName;

    public StringDataSource(IKey key) {
        super(key);
        threadName = this + " notify mediators";
    }

    @Override
    public String getData() {
        return value;
    }

    @Override
    public void setData(String avalue) {
        if ((value == null) || value.isEmpty() || (!value.equals(avalue))) {
            value = avalue;
            (new Thread(threadName) {
                @Override
                public void run() {
                    notifyMediators();
                };
            }).start();
        }

    }

    public String getState() {
        String state;
        if (value.isEmpty()) {
            state = UNKNOWN;
        } else {
            state = value;
        }
        return state;
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(String.class);
    }
}
