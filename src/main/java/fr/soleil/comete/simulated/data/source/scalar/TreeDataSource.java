//+============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/BooleanDAO.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.source.scalar;

import fr.soleil.comete.definition.widget.util.BasicTreeNode;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;

public class TreeDataSource extends AbstractDataSource<ITreeNode> {

    ITreeNode value = null;

    public TreeDataSource() {
        super(null);
    }

    @Override
    public ITreeNode getData() {
        return value;
    }

    @Override
    public void setData(ITreeNode avalue) {
        if (avalue != null && avalue != value) {
            value = avalue;
            notifyMediators();
        }
    }

    public String getState() {
        if (value == null) {
            return "ALARM";
        }
        else {
            return "VALID";
        }
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(Boolean.class);
    }

    public void setKey(IKey key) {
        ITreeNode data = new BasicTreeNode();
        data.setData(key);
        setData(data);
    }
}
