package fr.soleil.comete.simulated.data.plugin;

import fr.soleil.comete.definition.data.controller.ComponentColorController;
import fr.soleil.comete.definition.data.plugin.ComponentColorPlugin;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.data.adapter.source.DataSourceAdapter;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;

public class SimulatedQualityPlugin extends ComponentColorPlugin<Number> {

    public static final CometeColor ALARM_COLOR = CometeColor.RED;
    public static final CometeColor FAULT_COLOR = CometeColor.ORANGE;
    public static final CometeColor DEFAULT_COLOR = CometeColor.GREEN;

    public static final double SIMULATED_MAX_ALARM = 40;
    public static final double SIMULATED_MIN_ALARM = 0;
    public static final double SIMULATED_MAX_FAULT = 30;
    public static final double SIMULATED_MIN_FAULT = 10;

    public SimulatedQualityPlugin(IKey key) {
        super(key, new SimulatedQualityColorController());
    }

    public static class SimulatedQualityColorController extends ComponentColorController<Number> {

        public SimulatedQualityColorController() {
            super(Number.class);
        }

        @Override
        protected DataSourceAdapter<Number, CometeColor> initColorAdapter(
                AbstractDataSource<Number> source) {
            return new DataSourceAdapter<Number, CometeColor>(source, null) {

                @Override
                protected Number adaptContainerData(CometeColor data) {
                    return null;
                }

                @Override
                protected CometeColor adaptSourceData(Number data) {

                    CometeColor result = DEFAULT_COLOR;

                    double value = data.doubleValue();

                    if (value < SIMULATED_MIN_ALARM || value > SIMULATED_MAX_ALARM) {
                        result = ALARM_COLOR;
                    }
                    else if (value < SIMULATED_MIN_FAULT || value > SIMULATED_MAX_FAULT) {
                        result = FAULT_COLOR;
                    }

                    return result;
                }

            };
        }
    }

}
