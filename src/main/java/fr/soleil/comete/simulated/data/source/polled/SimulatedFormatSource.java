package fr.soleil.comete.simulated.data.source.polled;

import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;

public class SimulatedFormatSource extends AbstractDataSource<String> {

    private final FormatSource source;

    public SimulatedFormatSource(FormatSource source) {
        super(null);
        this.source = source;
    }

    @Override
    public String getData() {
        return source.getFormat();
    }

    @Override
    public void setData(String data) {
        // nothing to do
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(String.class);
    }
}
