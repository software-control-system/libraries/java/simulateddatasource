//+============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/StackDataArrayDataSource.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.source.chart;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import fr.soleil.comete.simulated.data.source.AbstractSimulatedDataSource;
import fr.soleil.comete.simulated.data.source.polled.FormatSource;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;

public class StackDataArrayDataSource extends AbstractSimulatedDataSource<List<Map<String, Object>>>
implements FormatSource {

    private List<Map<String, Object>> arrayList;
    private int listSize = 1;

    public StackDataArrayDataSource(IKey key) {
        super(key);
        arrayList = generateList();
    }

    @Override
    public List<Map<String, Object>> getData() {
        return arrayList;
    }

    @Override
    public void setData(List<Map<String, Object>> avalue) {
        // Nothing todo
    }

    public int getListSize() {
        return listSize;
    }

    private List<Map<String, Object>> generateList() {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < listSize; i++) {
            list.add(new NumberArrayDataSource(null).getData());
        }
        return Collections.unmodifiableList(list);
    }

    public void setListSize(int size) {
        listSize = size;
        arrayList = generateList();
    }

    public String getFormat() {
        return "%5.3f";
    }

    @Override
    public GenericDescriptor getDataType() {
        GenericDescriptor mapDesc = new GenericDescriptor(Map.class, new GenericDescriptor[] {
            new GenericDescriptor(String.class), new GenericDescriptor(Object.class) });

        return new GenericDescriptor(List.class, new GenericDescriptor[] { mapDesc });
    }

}
