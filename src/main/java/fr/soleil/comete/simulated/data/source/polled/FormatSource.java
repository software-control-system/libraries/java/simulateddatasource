package fr.soleil.comete.simulated.data.source.polled;

public interface FormatSource {

    public String getFormat();
}
