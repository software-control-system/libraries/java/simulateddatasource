package fr.soleil.comete.simulated.data.service;

import fr.soleil.data.service.IKey;

public interface IKeyGenerator {

    public IKey getNumberScalarKey();

    public IKey getStringScalarKey();

    public IKey getStringScalarKey2();

    public IKey getBooleanScalarKey();

    public IKey getSpectrumNumberMatrixKey();

    public IKey getImageNumberMatrixKey();

    public IKey getNumberMatrixListKey();

    public IKey getStringMatrixKey();

    public IKey getBooleanMatrixKey();

    public IKey getChartKey();

    public IKey getChartListKey();
}
