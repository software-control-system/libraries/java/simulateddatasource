//+============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/DoubleReadOnlyDataSource.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.source.matrix;

import fr.soleil.data.service.IKey;

public class DoubleImageRandomSizeDataSource extends AbstractDoubleMatrixRandomDataSource {

    protected static final int MIN_DIM = 50;

    private int maxWidth, maxHeight;

    public DoubleImageRandomSizeDataSource(IKey key) {
        this(key, 1000, 1000);
    }

    public DoubleImageRandomSizeDataSource(IKey key, int maxWidth, int maxHeight) {
        super(key, Math.max(maxWidth, MIN_DIM), Math.max(maxHeight, MIN_DIM));
        this.maxWidth = width;
        this.maxHeight = height;
    }

    protected int generateDim(int max) {
        int dim;
        if (max > MIN_DIM) {
            dim = MIN_DIM + (int) Math.floor(Math.random() * (max - MIN_DIM));
        } else {
            dim = MIN_DIM;
        }
        return dim;
    }

    @Override
    protected void generateImage() {
        width = generateDim(maxWidth);
        height = generateDim(maxHeight);
        int length = width * height;
        if (length != generatedReadValue.length) {
            generatedReadValue = new double[length];
        }
        super.generateImage();
    }
}
