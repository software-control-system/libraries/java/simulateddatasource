//+============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/DoubleDataSource.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.source.scalar;

import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;

public class DoubleDataSource extends AbstractNumberDataSource<Double> {

    private Double value = Double.valueOf(0);

    public DoubleDataSource(IKey key) {
        super(key);
    }

    @Override
    public Double getData() {
        return value;
    }

    @Override
    public void setData(Double avalue) {
        if (avalue.doubleValue() != value.doubleValue()) {
            value = avalue;
            notifyMediators();
        }
    }

    public String getFormat() {
        return "%5.2f";
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(Double.class);
    }

}
