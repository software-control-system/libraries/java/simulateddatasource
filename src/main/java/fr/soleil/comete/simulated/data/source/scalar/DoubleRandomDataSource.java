//+============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/DoubleReadOnlyDataSource.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.source.scalar;

import java.util.Random;

import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;

public class DoubleRandomDataSource extends AbstractNumberDataSource<Double> {

    private static Random random = new Random();

    public DoubleRandomDataSource(IKey key) {
        super(key);
    }

    @Override
    public Double getData() {
        return Double.valueOf(random.nextDouble());
    }

    @Override
    public void setData(Double avalue) {
        // do nothing
    }

    @Override
    public boolean isSettable() {
        return false;
    }

    public String getFormat() {
        return "%4.2f";
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(Double.class);
    }

}
