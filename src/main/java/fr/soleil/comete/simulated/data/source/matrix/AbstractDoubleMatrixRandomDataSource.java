//+============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/DoubleReadOnlyDataSource.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.source.matrix;

import java.util.Random;

import javax.activation.UnsupportedDataTypeException;

import fr.soleil.comete.simulated.data.source.AbstractSimulatedDataSource;
import fr.soleil.comete.simulated.data.source.polled.FormatSource;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.container.matrix.DoubleMatrix;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;

public abstract class AbstractDoubleMatrixRandomDataSource
        extends AbstractSimulatedDataSource<AbstractNumberMatrix<Double>> implements FormatSource {

    protected AbstractNumberMatrix<Double> matrix;
    protected int width;
    protected int height;
    protected double[] generatedReadValue;
    protected static Random random = new Random();
    protected boolean isWrite = false;

    public AbstractDoubleMatrixRandomDataSource(IKey key, int width, int height) {
        super(key);
        this.width = width;
        this.height = height;
        generatedReadValue = new double[width * height];
        matrix = new DoubleMatrix();
        matrix.setName("Random Image");
    }

    @Override
    public AbstractNumberMatrix<Double> getData() {
        if (!isWrite) {
            generateImage();
        }
        return matrix;
    }

    @Override
    public void setData(AbstractNumberMatrix<Double> avalue) {
        isWrite = true;
        matrix = avalue;
        notifyMediators();
    }

    protected int getAdaptedInt(int value) {
        int result = value;
        if (result < 1) {
            result = 1;
        }
        return result;
    }

    protected void generateImage() {
        int bimg_center_x = random.nextInt() % getAdaptedInt((int) (width * 0.050000000000000003D));
        if (random.nextLong() % 2L == 0L) {
            bimg_center_x *= -1;
        }
        int bimg_center_y = random.nextInt() % getAdaptedInt((int) (height * 0.050000000000000003D));
        if (random.nextInt() % 2 == 0) {
            bimg_center_y *= -1;
        }
        int bimg_x_offset_to_zero = ((width - 1) / 2) + bimg_center_x;
        int bimg_y_offset_to_zero = ((height - 1) / 2) + bimg_center_y;
        int xlimit = width / 8;
        int ylimit = height / 8;
        int xnoise = random.nextInt() % getAdaptedInt((int) (xlimit * 0.20000000000000001D));
        int ynoise = random.nextInt() % getAdaptedInt((int) (ylimit * 0.20000000000000001D));
        if (random.nextInt() % 2 == 0) {
            xnoise *= -1;
            ynoise *= -1;
        }
        xlimit += xnoise;
        ylimit += ynoise;
        for (int cpt = 0; cpt < width * height; cpt++) {
            generatedReadValue[cpt] = 0;
        }

        int limit = Math.max(xlimit, ylimit);

        for (int i = -limit; i < limit; i++) {
            int y = i + bimg_y_offset_to_zero;
            if ((y >= 0) && (y < height)) {
                for (int j = -limit; j < limit; j++) {
                    int x = j + bimg_x_offset_to_zero;
                    if ((x >= 0) && (x < width)) {
                        int value = (int) Math.sqrt(i * i + j * j);
                        generatedReadValue[y * width + x] = value >= limit ? 0 : limit - value;
                    }
                }

            }
        }
        try {
            matrix.setFlatValue(generatedReadValue, height, width);
        } catch (UnsupportedDataTypeException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getFormat() {
        return "%4.2f";
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(AbstractNumberMatrix.class,
                new GenericDescriptor[] { new GenericDescriptor(Double.TYPE) });
    }
}
