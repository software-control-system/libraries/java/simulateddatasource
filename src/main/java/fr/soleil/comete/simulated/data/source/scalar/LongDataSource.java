//+============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/DoubleDataSource.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.source.scalar;

import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;

public class LongDataSource extends AbstractNumberDataSource<Long> {
    private Long value = Long.valueOf(0);

    public LongDataSource(IKey key) {
        super(key);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Long getData() {
        return value;
    }

    @Override
    public void setData(Long avalue) {
        if (avalue.longValue() != value.longValue()) {
            value = avalue;
            notifyMediators();
        }
    }

    public String getFormat() {
        return "4.2f";
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(Long.class);
    }
}
