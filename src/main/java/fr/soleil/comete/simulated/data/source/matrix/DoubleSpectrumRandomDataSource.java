package fr.soleil.comete.simulated.data.source.matrix;

import javax.activation.UnsupportedDataTypeException;

import fr.soleil.data.service.IKey;

public class DoubleSpectrumRandomDataSource extends AbstractDoubleMatrixRandomDataSource {

    public DoubleSpectrumRandomDataSource(IKey key) {
        super(key, 1000, 1);
    }

    @Override
    protected void generateImage() {
        double[] flatValue = new double[height * width];
        for (int i = 0; i < flatValue.length; i++) {
            flatValue[i] = Math.random() * 100;
            if (i > 0) {
                flatValue[i] += flatValue[i - 1];
            }
        }
        try {
            matrix.setFlatValue(flatValue, height, width);
        } catch (UnsupportedDataTypeException e) {
            e.printStackTrace();
        }
    }

}
