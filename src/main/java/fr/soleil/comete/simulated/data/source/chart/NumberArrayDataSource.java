//+============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/DoubleDataSource.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.source.chart;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.simulated.data.source.AbstractSimulatedDataSource;
import fr.soleil.comete.simulated.data.source.polled.FormatSource;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;

public class NumberArrayDataSource extends AbstractSimulatedDataSource<Map<String, Object>> implements
FormatSource {

    private final Map<String, Object> dataMap = new LinkedHashMap<String, Object>();

    private static final Random random = new Random();
    private static final int arraySize = 30;

    public NumberArrayDataSource(IKey key) {
        super(key);

        double[][] dataArray = new double[2][arraySize];

        for (int i = 0; i < arraySize; i++) {
            dataArray[IChartViewer.Y_INDEX][i] = random.nextDouble();
            dataArray[IChartViewer.X_INDEX][i] = i + 1;
        }

        dataMap.put("SimulatedSpectrum", dataArray);
    }

    @Override
    public Map<String, Object> getData() {
        return dataMap;
    }

    @Override
    public void setData(Map<String, Object> avalue) {
        // Nothing to do
    }

    public String getFormat() {
        return "4.2f";
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(Map.class, new GenericDescriptor[] {
            new GenericDescriptor(String.class), new GenericDescriptor(Object.class) });
    }

}
