//+============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/BooleanDAO.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.source.scalar;

import fr.soleil.comete.simulated.data.source.AbstractSimulatedDataSource;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;

public class BooleanDataSource extends AbstractSimulatedDataSource<Boolean> {

    private Boolean value = false;

    public BooleanDataSource(IKey key) {
        super(key);

    }

    @Override
    public Boolean getData() {
        return value;
    }

    @Override
    public void setData(Boolean avalue) {
        if (avalue.booleanValue() != value.booleanValue()) {
            value = avalue;
            notifyMediators();
        }
    }

    public String getState() {
        if (value) {
            return "ALARM";
        }
        else {
            return "VALID";
        }
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(Boolean.class);
    }

}
