//+============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/NumberRandomDataSource.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.source.scalar;

import java.util.Random;

import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;

public class NumberRandomDataSource extends AbstractNumberDataSource<Number> {
    private static Random random = new Random();
    private Number value = null;

    public NumberRandomDataSource(IKey key) {
        super(key);
    }

    @Override
    public Number getData() throws Exception {
        Number result;
        if (value == null) {
            double dvalue = random.nextDouble();
            // System.out.println(dvalue);
            Double doubleObject = Double.valueOf(dvalue);
            // System.out.println(doubleObject.toString());
            result = doubleObject;
        }
        else {
            result = value;
        }
        return result;
    }

    @Override
    public void setData(Number avalue) {
        value = avalue;
        notifyMediators();
    }

    public String getFormat() {
        return "%4.2f";
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(Number.class);
    }
}
