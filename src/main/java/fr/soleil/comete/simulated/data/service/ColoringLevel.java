package fr.soleil.comete.simulated.data.service;

/**
 * A simulated coloring level, to generate some various color plugin behaviours
 * 
 * @author girardot
 */
public enum ColoringLevel {
    QUALITY, STATE
}
