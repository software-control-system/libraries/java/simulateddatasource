//+============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/SimulatedDAOFactory.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.service;

import java.util.List;
import java.util.Map;

import fr.soleil.comete.simulated.data.source.chart.DataArrayDataSource;
import fr.soleil.comete.simulated.data.source.chart.StackDataArrayDataSource;
import fr.soleil.comete.simulated.data.source.matrix.BooleanMatrixDataSource;
import fr.soleil.comete.simulated.data.source.matrix.DoubleImageRandomDataSource;
import fr.soleil.comete.simulated.data.source.matrix.DoubleSpectrumRandomDataSource;
import fr.soleil.comete.simulated.data.source.matrix.StackMatrixRandomDataSource;
import fr.soleil.comete.simulated.data.source.matrix.StringMatrixDataSource;
import fr.soleil.comete.simulated.data.source.polled.FormatSource;
import fr.soleil.comete.simulated.data.source.polled.SimulatedFormatSource;
import fr.soleil.comete.simulated.data.source.scalar.BooleanDataSource;
import fr.soleil.comete.simulated.data.source.scalar.BooleanRandomDataSource;
import fr.soleil.comete.simulated.data.source.scalar.DoubleDataSource;
import fr.soleil.comete.simulated.data.source.scalar.DoubleRandomDataSource;
import fr.soleil.comete.simulated.data.source.scalar.StringDataSource;
import fr.soleil.comete.simulated.data.source.scalar.TreeDataSource;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;

public class SimulatedDataSourceProducer implements IDataSourceProducer {

    public static final String SOURCE_PRODUCER_ID = "fr.soleil.comete.simuled";

    public static final SimulatedKeyGenerator GENERATOR = new SimulatedKeyGenerator();

    private final BooleanMatrixDataSource booleanMatrixDataSource = new BooleanMatrixDataSource(
            GENERATOR.getBooleanMatrixKey());
    private final DataArrayDataSource dataArrayDataSource = new DataArrayDataSource(GENERATOR.getChartKey());

    // TODO Activate following code and deactivate next one if you prefer have images with variable size.
//    private final DoubleImageRandomSizeDataSource doubleImageDataSource = new DoubleImageRandomSizeDataSource(
//            GENERATOR.getImageNumberMatrixKey(), 1000, 1000);
    private final DoubleImageRandomDataSource doubleImageDataSource = new DoubleImageRandomDataSource(
            GENERATOR.getImageNumberMatrixKey());

    private final DoubleSpectrumRandomDataSource doubleSpectrumDataSource = new DoubleSpectrumRandomDataSource(
            GENERATOR.getSpectrumNumberMatrixKey());
    private final StringDataSource stringDataSource = new StringDataSource(GENERATOR.getStringScalarKey());
    private final StringMatrixDataSource stringMatrixDataSource = new StringMatrixDataSource(
            GENERATOR.getStringMatrixKey());
    private final StackDataArrayDataSource stackDataArrayDataSource;
    private final StackMatrixRandomDataSource stackMatrixRandomDataSource;
    private final TreeDataSource treeDataSource = new TreeDataSource();

    public SimulatedDataSourceProducer() {
        stackDataArrayDataSource = (StackDataArrayDataSource) createStackNumberDataArrayDataSource();
        stackMatrixRandomDataSource = (StackMatrixRandomDataSource) createStackNumberImageDataSource();
    }

    @Override
    public String getId() {
        return SOURCE_PRODUCER_ID;
    }

    @Override
    public AbstractDataSource<?> createDataSource(IKey key) {
        AbstractDataSource<?> result = null;
        SimulatedKey parsedKey = (SimulatedKey) parseKey(key);
        if (parsedKey != null) {
            GenericDescriptor sourceType = null;
            if (SimulatedKey.isADescriptor(parsedKey)) {
                sourceType = SimulatedKey.getDescriptor(parsedKey);
            } else {
                sourceType = parsedKey.getExpectedSourceType();
            }
            if (SimulatedKey.isTreeKey(parsedKey)) {
                treeDataSource.setKey(parsedKey);
                result = treeDataSource;
            } else if (sourceType != null) {
                // A DataSource can be constructed only if the key is correctly
                // parametered.
                if (Boolean.class.isAssignableFrom(sourceType.getConcernedClass())) {
                    if (parsedKey.isRandom()) {
                        result = new BooleanRandomDataSource(parsedKey);
                    } else {
                        result = new BooleanDataSource(parsedKey);
                    }
                } else if (String.class.isAssignableFrom(sourceType.getConcernedClass())) {
                    result = stringDataSource;
                } else if (Number.class.isAssignableFrom(sourceType.getConcernedClass())) {
                    if (parsedKey.isRandom()) {
                        result = new DoubleRandomDataSource(parsedKey);
                    } else {
                        result = new DoubleDataSource(parsedKey);
                    }
                } else if (AbstractMatrix.class.isAssignableFrom(sourceType.getConcernedClass())) {
                    GenericDescriptor[] parameters = sourceType.getParameters();
                    for (GenericDescriptor desc : parameters) {
                        if (Boolean.class.isAssignableFrom(desc.getConcernedClass())) {
                            result = booleanMatrixDataSource;
                            break;
                        }
                        if (String.class.isAssignableFrom(desc.getConcernedClass())) {
                            result = stringMatrixDataSource;
                            break;
                        }
                        if (Number.class.isAssignableFrom(desc.getConcernedClass())) {
                            if (parsedKey.isSpectrum()) {
                                result = doubleSpectrumDataSource;
                            } else {
                                result = doubleImageDataSource;
                            }
                            break;
                        }
                    }
                } else if (List.class.isAssignableFrom(sourceType.getConcernedClass())) {
                    GenericDescriptor[] parameters = sourceType.getParameters();
                    for (GenericDescriptor desc : parameters) {
                        if (Map.class.isAssignableFrom(desc.getConcernedClass())) {
                            GenericDescriptor[] subParameters = desc.getParameters();
                            if (String.class.isAssignableFrom(subParameters[0].getConcernedClass())) {
                                result = stackDataArrayDataSource;
                            }
                        } else if (AbstractNumberMatrix.class.isAssignableFrom(desc.getConcernedClass())) {
                            result = stackMatrixRandomDataSource;
                        }
                    }
                } else if (Map.class.isAssignableFrom(sourceType.getConcernedClass())) {
                    GenericDescriptor[] parameters = sourceType.getParameters();
                    if (String.class.isAssignableFrom(parameters[0].getConcernedClass())) {
                        result = dataArrayDataSource;
                    }
                }
            }
            if (result != null) {
                if (SimulatedKey.isAFormat(key) && (result instanceof FormatSource)) {
                    result = new SimulatedFormatSource((FormatSource) result);
                }
                // else if (SimulatedKey.isAQuality(key)) {
                // // TODO
                // }
                // else if (SimulatedKey.isAState(key)) {
                // // TODO
                // }
            }

        }
        return result;
    }

    public AbstractDataSource<List<Map<String, Object>>> createStackNumberDataArrayDataSource() {
        StackDataArrayDataSource dao = new StackDataArrayDataSource(GENERATOR.getChartListKey());
        dao.setListSize(50);
        return dao;
    }

    public AbstractDataSource<List<AbstractNumberMatrix<?>>> createStackNumberImageDataSource() {
        StackMatrixRandomDataSource dao = new StackMatrixRandomDataSource(GENERATOR.getNumberMatrixListKey());
        dao.setListSize(30);
        return dao;
    }

    @Override
    public IKey parseKey(IKey key) {
        IKey newKey = null;
        if (key instanceof SimulatedKey) {
            newKey = key;
        }
        return newKey;
    }

    @Override
    public boolean isSourceSettable(IKey key) {
        boolean settable = false;
        if (key instanceof SimulatedKey) {
            settable = ((SimulatedKey) key).isSettable();
        }
        return settable;
    }

    @Override
    public boolean isSourceCreatable(IKey key) {
        SimulatedKey parsedKey = (SimulatedKey) parseKey(key);
        return (parsedKey != null);
    }

    @Override
    public String getName() {
        return "Simulated";
    }

    @Override
    public int getRank(IKey key) {
        int rank = -1;
        SimulatedKey parsedKey = (SimulatedKey) parseKey(key);
        if (parsedKey != null) {
            if (!SimulatedKey.isTreeKey(parsedKey)) {
                GenericDescriptor sourceType = null;
                if (SimulatedKey.isADescriptor(parsedKey)) {
                    sourceType = SimulatedKey.getDescriptor(parsedKey);
                } else {
                    sourceType = parsedKey.getExpectedSourceType();
                }
                if (sourceType != null) {
                    if (Boolean.class.isAssignableFrom(sourceType.getConcernedClass())) {
                        rank = 0;
                    } else if (String.class.isAssignableFrom(sourceType.getConcernedClass())) {
                        rank = 0;
                    } else if (Number.class.isAssignableFrom(sourceType.getConcernedClass())) {
                        rank = 0;
                    } else if (AbstractMatrix.class.isAssignableFrom(sourceType.getConcernedClass())) {
                        if (parsedKey.isSpectrum()) {
                            rank = 1;
                        } else {
                            rank = 2;
                        }
                    } else if (List.class.isAssignableFrom(sourceType.getConcernedClass())) {
                        rank = 1;
                    } else if (Map.class.isAssignableFrom(sourceType.getConcernedClass())) {
                        rank = 1;
                    }
                }
            }
        }
        return rank;
    }

    @Override
    public int[] getShape(IKey key) {
        // can't be calculated
        return null;
    }
}
