//+============================================================================
//Source: package fr.soleil.comete.dao.simulateddao;/DoubleDataSource.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.simulated.data.source.matrix;

import javax.activation.UnsupportedDataTypeException;

import fr.soleil.comete.simulated.data.source.AbstractSimulatedDataSource;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.container.matrix.StringMatrix;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;

public class StringMatrixDataSource extends AbstractSimulatedDataSource<AbstractMatrix<String>> {

    private AbstractMatrix<String> value = new StringMatrix();

    public StringMatrixDataSource(IKey key) {
        super(key);
        try {
            value.setFlatValue(
                    new String[] { "Machin", "Toto", "Hello", "Machin", "Toto", "Hello" }, 3, 2);
        }
        catch (UnsupportedDataTypeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public AbstractMatrix<String> getData() {
        return value;
    }

    @Override
    public void setData(AbstractMatrix<String> avalue) {
        value = avalue;
        notifyMediators();
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(AbstractMatrix.class,
                new GenericDescriptor[] { new GenericDescriptor(String.class) });
    }

}
